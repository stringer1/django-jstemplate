from django import template
from .base import BaseJSTemplateNode, jstemplate_tag_helper



register = template.Library()



class EmberNode(BaseJSTemplateNode):
    def generate_node_text(self, resolved_name, file_content):
        output = ('<script type="text/x-handlebars" id="{0}" data-template-name="{0}">'.format(resolved_name)
                    + file_content + '</script>')
        return output



@register.tag
def ember(parser, token):
    """
    Finds the Handlebars template for the given name and renders it surrounded
    by script tags, tagged properly for Ember.js.

    """
    return jstemplate_tag_helper('ember', EmberNode,
                                 parser, token)
